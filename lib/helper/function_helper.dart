part of 'helper.dart';

String? encodeQueryParameters(Map<String, String> params) {
  return params.entries
      .map((e) =>
          '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
      .join('&');
}

String dateToReadable(String date) {
  String finalString = '';

  List<String> breakDate = date.split('-');

  switch (breakDate[1]) {
    case '01':
      finalString = finalString + 'Jan';
      break;
    case '02':
      finalString = finalString + 'Feb';
      break;
    case '03':
      finalString = finalString + 'Mar';
      break;
    case '04':
      finalString = finalString + 'Apr';
      break;
    case '05':
      finalString = finalString + 'May';
      break;
    case '06':
      finalString = finalString + 'Jun';
      break;
    case '07':
      finalString = finalString + 'Jul';
      break;
    case '08':
      finalString = finalString + 'Aug';
      break;
    case '09':
      finalString = finalString + 'Sep';
      break;
    case '10':
      finalString = finalString + 'Oct';
      break;
    case '11':
      finalString = finalString + 'Nov';
      break;
    case '12':
      finalString = finalString + 'Dec';
      break;
    default:
  }

  finalString = breakDate[0] + ' $finalString ' + breakDate[2];

  return finalString;
}
