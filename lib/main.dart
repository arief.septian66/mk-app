import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:mk_app/pages/pages.dart';

ValueNotifier<bool> isConnected = ValueNotifier(true);

void main() {
  InternetConnectionChecker().onStatusChange.listen((status) {
    switch (status) {
      case InternetConnectionStatus.connected:
        isConnected.value = true;
        break;
      case InternetConnectionStatus.disconnected:
        isConnected.value = false;
        break;
    }
  });

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MK APP',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomeScreenPage(),
    );
  }
}
