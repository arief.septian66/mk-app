import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mk_app/helper/helper.dart';
import 'package:mk_app/models/models.dart';
import 'package:mk_app/pages/pages.dart';
import 'package:shimmer/shimmer.dart';

class CandidatesInfoWidget extends StatelessWidget {
  final Candidate data;
  const CandidatesInfoWidget({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 80,
          height: 80,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  fit: BoxFit.cover, image: NetworkImage(data.photo))),
        ),
        SizedBox(width: 16),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              data.name,
              style: mainFont.copyWith(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.black87),
            ),
            Text(
              data.gender == 'm' ? 'Male' : 'Female',
              style: mainFont.copyWith(fontSize: 14, color: Colors.black87),
            ),
            SizedBox(height: 8),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Birthday',
                  style: mainFont.copyWith(fontSize: 12, color: Colors.black87),
                ),
                Text(
                  dateToReadable(
                      DateFormat('dd-MM-yyyy').format(data.birthday)),
                  style: mainFont.copyWith(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Colors.black87),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Expired',
                  style: mainFont.copyWith(fontSize: 12, color: Colors.black87),
                ),
                Text(
                  dateToReadable(DateFormat('dd-MM-yyyy').format(data.expired)),
                  style: mainFont.copyWith(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Colors.black87),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Expired Status',
                  style: mainFont.copyWith(fontSize: 12, color: Colors.black87),
                ),
                Text(
                  data.expired.isBefore(DateTime.now())
                      ? 'Expired'
                      : 'Non-Expired',
                  style: mainFont.copyWith(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: data.expired.isBefore(DateTime.now())
                          ? Colors.red
                          : Colors.green),
                )
              ],
            )
          ],
        ))
      ],
    );
  }
}

class BlogPreviewWidget extends StatelessWidget {
  final Blog data;
  final int index;
  const BlogPreviewWidget({Key? key, required this.data, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (_) => BlogDetailPage(data: data)));
      },
      child: Container(
        margin: EdgeInsets.only(left: index == 0 ? 0 : 4),
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Column(
            children: [
              Container(
                width: 150,
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10)),
                    image: DecorationImage(
                        fit: BoxFit.cover, image: NetworkImage(data.photo))),
              ),
              Container(
                width: 150,
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: Column(
                  children: [
                    Text(
                      data.title,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: mainFont.copyWith(
                          fontSize: 11,
                          color: Colors.black87,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 4),
                    Text(
                      data.subTitle,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: mainFont.copyWith(
                        fontSize: 11,
                        color: Colors.black87,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class PlaceHolder extends StatelessWidget {
  final Widget child;
  const PlaceHolder({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        baseColor: Colors.black87.withOpacity(0.1),
        highlightColor: Colors.white,
        child: child);
  }
}
