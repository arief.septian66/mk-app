part of 'blocs.dart';

class CandidateBloc {
  MasterModel<List<Candidate>> dataCandidate =
      MasterModel(data: [], isError: false, isLoading: false);

  BehaviorSubject<MasterModel<List<Candidate>>> _subjectCandidate =
      BehaviorSubject<MasterModel<List<Candidate>>>.seeded(
          MasterModel(data: [], isError: false, isLoading: false));

  dynamic get candidateObservable => _subjectCandidate.stream;

  void intialLoad() async {
    dataCandidate = MasterModel(data: [], isError: false, isLoading: true);
    _subjectCandidate.sink.add(dataCandidate);

    dataCandidate = await CandidateRepository.getCandidate();

    _subjectCandidate.sink.add(dataCandidate);
  }

  void searchCandidateByQuery({required String query}) async {
    dataCandidate = MasterModel(data: [], isError: false, isLoading: true);
    _subjectCandidate.sink.add(dataCandidate);

    dataCandidate = await CandidateRepository.getCandidateByQuery(query: query);

    _subjectCandidate.sink.add(dataCandidate);
  }
}
