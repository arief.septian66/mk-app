import 'package:mk_app/models/models.dart';
import 'package:mk_app/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

part 'blog_bloc.dart';
part 'candidate_bloc.dart';
part 'address_bloc.dart';
part 'experience_bloc.dart';
part 'contact_bloc.dart';
