part of 'blocs.dart';

class AddressBloc {
  MasterModel<Address?> data =
      MasterModel(data: null, isError: false, isLoading: false);

  BehaviorSubject<MasterModel<Address?>> _subjectAddress =
      BehaviorSubject<MasterModel<Address?>>.seeded(
          MasterModel(data: null, isError: false, isLoading: false));

  dynamic get addressObservable => _subjectAddress.stream;

  void searchAddressById({required int id}) async {
    data = MasterModel(data: null, isError: false, isLoading: true);
    _subjectAddress.sink.add(data);

    data = await AddressRepository.getAddressById(id: id);

    _subjectAddress.sink.add(data);
  }
}
