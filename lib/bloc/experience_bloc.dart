part of 'blocs.dart';

class ExperienceBloc {
  MasterModel<Experience?> data =
      MasterModel(data: null, isError: false, isLoading: false);

  BehaviorSubject<MasterModel<Experience?>> _subjectExperience =
      BehaviorSubject<MasterModel<Experience?>>.seeded(
          MasterModel(data: null, isError: false, isLoading: false));

  dynamic get experieceObservable => _subjectExperience.stream;

  void searchExperienceById({required int id}) async {
    data = MasterModel(data: null, isError: false, isLoading: true);
    _subjectExperience.sink.add(data);

    data = await ExperienceRepository.getExperienceById(id: id);

    _subjectExperience.sink.add(data);
  }
}
