part of 'blocs.dart';

class BlogBloc {
  MasterModel<List<Blog>> dataBlog =
      MasterModel(data: [], isError: false, isLoading: false);

  BehaviorSubject<MasterModel<List<Blog>>> _subjectBlog =
      BehaviorSubject<MasterModel<List<Blog>>>.seeded(
          MasterModel(data: [], isError: false, isLoading: false));

  dynamic get blogObservable => _subjectBlog.stream;

  void intialLoad() async {
    dataBlog = MasterModel(data: [], isError: false, isLoading: true);
    _subjectBlog.sink.add(dataBlog);

    dataBlog = await BlogRepository.getBlog();
    _subjectBlog.sink.add(dataBlog);
  }

  void searchBlogByQuery({required String query}) async {
    dataBlog = MasterModel(data: [], isError: false, isLoading: true);
    _subjectBlog.sink.add(dataBlog);

    dataBlog = await BlogRepository.getBlogByQuery(query: query);
    _subjectBlog.sink.add(dataBlog);
  }
}
