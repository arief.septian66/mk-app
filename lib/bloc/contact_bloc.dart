part of 'blocs.dart';

class ContactBloc {
  MasterModel<Contact?> data =
      MasterModel(data: null, isError: false, isLoading: false);

  BehaviorSubject<MasterModel<Contact?>> _subjectContact =
      BehaviorSubject<MasterModel<Contact?>>.seeded(
          MasterModel(data: null, isError: false, isLoading: false));

  dynamic get contactObservable => _subjectContact.stream;

  void searchContactById({required int id}) async {
    data = MasterModel(data: null, isError: false, isLoading: true);
    _subjectContact.sink.add(data);

    data = await ContactRepository.getContactById(id: id);

    _subjectContact.sink.add(data);
  }
}
