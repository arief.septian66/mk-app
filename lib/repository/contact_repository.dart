part of 'repository.dart';

class ContactRepository {
  static Future<MasterModel<Contact?>> getContactById({required int id}) async {
    String dataRaw = await rootBundle.loadString('assets/emails.json');
    final dataJson = json.decode(dataRaw);

    MasterModel<Contact?> dataReturn =
        MasterModel(data: null, isError: false, isLoading: false);

    await Future.delayed(const Duration(seconds: 2));

    if (isConnected.value) {
      for (int i = 0; i < dataJson['results'].length; i++) {
        Contact data = Contact.fromJson(dataJson['results'][i]);
        if (data.id == id) {
          dataReturn.data = data;
        }
      }

      dataReturn.isError = false;
    } else {
      dataReturn.isError = true;
    }

    return dataReturn;
  }
}
