part of 'repository.dart';

class AddressRepository {
  static Future<MasterModel<Address?>> getAddressById({required int id}) async {
    String dataRaw = await rootBundle.loadString('assets/address.json');
    final dataJson = json.decode(dataRaw);

    MasterModel<Address?> dataReturn =
        MasterModel(data: null, isError: false, isLoading: false);

    await Future.delayed(const Duration(seconds: 2));

    if (isConnected.value) {
      for (int i = 0; i < dataJson['results'].length; i++) {
        Address data = Address.fromJson(dataJson['results'][i]);
        if (data.id == id) {
          dataReturn.data = data;
        }
      }

      dataReturn.isError = false;
    } else {
      dataReturn.isError = true;
    }

    return dataReturn;
  }
}
