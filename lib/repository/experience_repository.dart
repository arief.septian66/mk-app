part of 'repository.dart';

class ExperienceRepository {
  static Future<MasterModel<Experience?>> getExperienceById(
      {required int id}) async {
    String dataRaw = await rootBundle.loadString('assets/experiences.json');
    final dataJson = json.decode(dataRaw);

    MasterModel<Experience?> dataReturn =
        MasterModel(data: null, isError: false, isLoading: false);

    await Future.delayed(const Duration(seconds: 2));

    if (isConnected.value) {
      for (int i = 0; i < dataJson['results'].length; i++) {
        Experience data = Experience.fromJson(dataJson['results'][i]);
        if (data.id == id) {
          dataReturn.data = data;
        }
      }

      dataReturn.isError = false;
    } else {
      dataReturn.isError = true;
    }

    return dataReturn;
  }
}
