import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:mk_app/main.dart';
import 'package:mk_app/models/models.dart';

part 'blog_repository.dart';
part 'candidate_repository.dart';
part 'address_repository.dart';
part 'experience_repository.dart';
part 'contact_repository.dart';
