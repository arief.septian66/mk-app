part of 'repository.dart';

class BlogRepository {
  static Future<MasterModel<List<Blog>>> getBlog() async {
    String dataRaw = await rootBundle.loadString('assets/blogs.json');
    final dataJson = json.decode(dataRaw);

    MasterModel<List<Blog>> dataReturn =
        MasterModel(data: [], isError: false, isLoading: false);

    await Future.delayed(const Duration(seconds: 2));

    if (isConnected.value) {
      List<Blog> dataBlog = [];
      for (int i = 0; i < dataJson['results'].length; i++) {
        dataBlog.add(Blog.fromJson(dataJson['results'][i]));
      }

      dataReturn.data = dataBlog;
      dataReturn.isError = false;
    } else {
      dataReturn.isError = true;
    }

    return dataReturn;
  }

  static Future<MasterModel<List<Blog>>> getBlogByQuery(
      {required String query}) async {
    String dataRaw = await rootBundle.loadString('assets/blogs.json');
    final dataJson = json.decode(dataRaw);

    MasterModel<List<Blog>> dataReturn =
        MasterModel(data: [], isError: false, isLoading: false);

    await Future.delayed(const Duration(seconds: 2));

    if (isConnected.value) {
      List<Blog> dataBlog = [];
      for (int i = 0; i < dataJson['results'].length; i++) {
        Blog data = Blog.fromJson(dataJson['results'][i]);
        if (data.title.toLowerCase().contains(query)) {
          dataBlog.add(data);
        }
      }

      dataReturn.data = dataBlog;
      dataReturn.isError = false;
    } else {
      dataReturn.isError = true;
    }

    return dataReturn;
  }
}
