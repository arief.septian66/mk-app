part of 'repository.dart';

class CandidateRepository {
  static Future<MasterModel<List<Candidate>>> getCandidate() async {
    String dataRaw = await rootBundle.loadString('assets/candidates.json');
    final dataJson = json.decode(dataRaw);

    MasterModel<List<Candidate>> dataReturn =
        MasterModel(data: [], isError: false, isLoading: false);

    await Future.delayed(const Duration(seconds: 2));

    if (isConnected.value) {
      List<Candidate> dataCandidate = [];
      for (int i = 0; i < dataJson['results'].length; i++) {
        dataCandidate.add(Candidate.fromJson(dataJson['results'][i]));
      }

      dataReturn.data = dataCandidate;
      dataReturn.isError = false;
    } else {
      dataReturn.isError = true;
    }

    return dataReturn;
  }

  static Future<MasterModel<List<Candidate>>> getCandidateByQuery(
      {required String query}) async {
    String dataRaw = await rootBundle.loadString('assets/candidates.json');
    final dataJson = json.decode(dataRaw);

    MasterModel<List<Candidate>> dataReturn =
        MasterModel(data: [], isError: false, isLoading: false);

    await Future.delayed(const Duration(seconds: 2));

    if (isConnected.value) {
      List<Candidate> dataCandidate = [];
      for (int i = 0; i < dataJson['results'].length; i++) {
        Candidate data = Candidate.fromJson(dataJson['results'][i]);
        if (data.name.toLowerCase().contains(query)) {
          dataCandidate.add(data);
        }
      }

      dataReturn.data = dataCandidate;
      dataReturn.isError = false;
    } else {
      dataReturn.isError = true;
    }

    return dataReturn;
  }
}
