import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mk_app/bloc/blocs.dart';
import 'package:mk_app/helper/helper.dart';
import 'package:mk_app/main.dart';
import 'package:mk_app/models/models.dart';
import 'package:mk_app/widgets/candidates_info_widget.dart';
import 'package:url_launcher/url_launcher.dart';

part 'home_screen/home_screen.dart';
part 'home_screen/home_screen_search_section.dart';

part 'candidates_detail_page.dart';
part 'blog_detail_page.dart';
part 'search_page.dart';
