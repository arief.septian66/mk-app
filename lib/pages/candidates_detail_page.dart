part of 'pages.dart';

class CandidateDetailPage extends StatefulWidget {
  final Candidate data;
  const CandidateDetailPage({Key? key, required this.data}) : super(key: key);

  @override
  State<CandidateDetailPage> createState() => _CandidateDetailPageState();
}

class _CandidateDetailPageState extends State<CandidateDetailPage> {
  AddressBloc addressBloc = AddressBloc();
  ExperienceBloc experienceBloc = ExperienceBloc();
  ContactBloc contactBloc = ContactBloc();

  @override
  void initState() {
    addressBloc.searchAddressById(id: widget.data.id);
    experienceBloc.searchExperienceById(id: widget.data.id);
    contactBloc.searchContactById(id: widget.data.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        title: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            children: [
              const Icon(
                Icons.arrow_back,
                color: Colors.black87,
              ),
              SizedBox(width: 15),
              Expanded(
                child: Text(
                  'Candidate',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: mainFont.copyWith(
                      color: Colors.black87,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 16),
      children: [
        SizedBox(height: 16),
        Text(
          'Candidate Profile',
          style: mainFont.copyWith(
              fontSize: 20, color: Colors.black87, fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 8),
        CandidatesInfoWidget(data: widget.data),
        SizedBox(height: 16),
        Text(
          'Candidate Work Status',
          style: mainFont.copyWith(
              fontSize: 20, color: Colors.black87, fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 8),
        StreamBuilder(
            stream: experienceBloc.experieceObservable,
            builder:
                (context, AsyncSnapshot<MasterModel<Experience?>> snapshot) {
              return snapshot.data == null
                  ? Container()
                  : snapshot.data!.isLoading
                      ? PlaceHolder(
                          child: Container(
                            width: double.infinity,
                            height: 100,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                          ),
                        )
                      : snapshot.data!.isError
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Something Wrong, Please Try Again',
                                  style: mainFont.copyWith(fontSize: 14),
                                ),
                                SizedBox(height: 8),
                                GestureDetector(
                                  onTap: () {
                                    experienceBloc.searchExperienceById(
                                        id: widget.data.id);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 16, vertical: 8),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Theme.of(context).primaryColor),
                                    child: Text(
                                      'Retry',
                                      style: mainFont.copyWith(
                                          fontSize: 14, color: Colors.white),
                                    ),
                                  ),
                                )
                              ],
                            )
                          : Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 14, horizontal: 16),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            snapshot.data!.data!.companyName,
                                            style: mainFont.copyWith(
                                                fontSize: 16,
                                                color: Colors.black87,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 6,
                                          ),
                                          Text(snapshot.data!.data!.jobTitle,
                                              style: mainFont.copyWith(
                                                fontSize: 16,
                                                color: Colors.black87,
                                              )),
                                          Text(
                                              'Industry : ' +
                                                  snapshot.data!.data!.industry,
                                              style: mainFont.copyWith(
                                                fontSize: 12,
                                                color: Colors.black54,
                                              ))
                                        ],
                                      ),
                                    ),
                                    SizedBox(width: 8),
                                    Text(
                                      snapshot.data!.data!.status,
                                      style: mainFont.copyWith(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                          color:
                                              Theme.of(context).primaryColor),
                                    )
                                  ],
                                ),
                              ),
                            );
            }),
        SizedBox(height: 16),
        Text(
          'Candidate Address',
          style: mainFont.copyWith(
              fontSize: 20, color: Colors.black87, fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 8),
        StreamBuilder(
            stream: addressBloc.addressObservable,
            builder: (context, AsyncSnapshot<MasterModel<Address?>> snapshot) {
              return snapshot.data == null
                  ? Container()
                  : snapshot.data!.isLoading
                      ? PlaceHolder(
                          child: Container(
                            width: double.infinity,
                            height: 100,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                          ),
                        )
                      : snapshot.data!.isError
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Something Wrong, Please Try Again',
                                  style: mainFont.copyWith(fontSize: 14),
                                ),
                                SizedBox(height: 8),
                                GestureDetector(
                                  onTap: () {
                                    addressBloc.searchAddressById(
                                        id: widget.data.id);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 16, vertical: 8),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Theme.of(context).primaryColor),
                                    child: Text(
                                      'Retry',
                                      style: mainFont.copyWith(
                                          fontSize: 14, color: Colors.white),
                                    ),
                                  ),
                                )
                              ],
                            )
                          : Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 14, horizontal: 16),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      snapshot.data!.data!.address,
                                      style: mainFont.copyWith(
                                        fontSize: 14,
                                        color: Colors.black87,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 6,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'City',
                                          style: mainFont.copyWith(
                                              fontSize: 14,
                                              color: Colors.black54),
                                        ),
                                        Text(
                                          snapshot.data!.data!.city,
                                          style: mainFont.copyWith(
                                              fontSize: 14,
                                              color: Colors.black87),
                                        )
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'State',
                                          style: mainFont.copyWith(
                                              fontSize: 14,
                                              color: Colors.black54),
                                        ),
                                        Text(
                                          snapshot.data!.data!.state,
                                          style: mainFont.copyWith(
                                              fontSize: 14,
                                              color: Colors.black87),
                                        )
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Zip Code',
                                          style: mainFont.copyWith(
                                              fontSize: 14,
                                              color: Colors.black54),
                                        ),
                                        Text(
                                          snapshot.data!.data!.zipCode,
                                          style: mainFont.copyWith(
                                              fontSize: 14,
                                              color: Colors.black87),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            );
            }),
        SizedBox(height: 16),
        Text(
          'Candidate Contact',
          style: mainFont.copyWith(
              fontSize: 20, color: Colors.black87, fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 8),
        StreamBuilder(
            stream: contactBloc.contactObservable,
            builder: (context, AsyncSnapshot<MasterModel<Contact?>> snapshot) {
              return snapshot.data == null
                  ? Container()
                  : snapshot.data!.isLoading
                      ? PlaceHolder(
                          child: Container(
                            width: double.infinity,
                            height: 100,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                          ),
                        )
                      : snapshot.data!.isError
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Something Wrong, Please Try Again',
                                  style: mainFont.copyWith(fontSize: 14),
                                ),
                                SizedBox(height: 8),
                                GestureDetector(
                                  onTap: () {
                                    contactBloc.searchContactById(
                                        id: widget.data.id);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 16, vertical: 8),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Theme.of(context).primaryColor),
                                    child: Text(
                                      'Retry',
                                      style: mainFont.copyWith(
                                          fontSize: 14, color: Colors.white),
                                    ),
                                  ),
                                )
                              ],
                            )
                          : Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 14, horizontal: 16),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                            child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Email',
                                              style: mainFont.copyWith(
                                                  fontSize: 14,
                                                  color: Colors.black54),
                                            ),
                                            Text(
                                              snapshot.data!.data!.email,
                                              style: mainFont.copyWith(
                                                fontSize: 14,
                                                color: Colors.black87,
                                              ),
                                            ),
                                          ],
                                        )),
                                        SizedBox(width: 16),
                                        GestureDetector(
                                          onTap: () {
                                            final Uri emailLaunchUri = Uri(
                                              scheme: 'mailto',
                                              path: snapshot.data!.data!.email,
                                              query: encodeQueryParameters(<
                                                  String, String>{
                                                'subject': 'Hi i am MK company'
                                              }),
                                            );

                                            launchUrl(emailLaunchUri);
                                          },
                                          child: Container(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 8, horizontal: 16),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  color: Theme.of(context)
                                                      .primaryColor),
                                              child: Row(
                                                children: [
                                                  Icon(
                                                    Icons.mail,
                                                    color: Colors.white,
                                                  ),
                                                  SizedBox(width: 8),
                                                  Text(
                                                    'Email',
                                                    style: mainFont.copyWith(
                                                        fontSize: 14,
                                                        color: Colors.white),
                                                  ),
                                                ],
                                              )),
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 16),
                                    Row(
                                      children: [
                                        Expanded(
                                            child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Phone Number',
                                              style: mainFont.copyWith(
                                                  fontSize: 14,
                                                  color: Colors.black54),
                                            ),
                                            Text(
                                              snapshot.data!.data!.phoneNumber,
                                              style: mainFont.copyWith(
                                                fontSize: 14,
                                                color: Colors.black87,
                                              ),
                                            ),
                                          ],
                                        )),
                                        SizedBox(width: 16),
                                        GestureDetector(
                                          onTap: () {
                                            String phoneNumber = snapshot
                                                .data!.data!.phoneNumber
                                                .replaceAll(' ', '')
                                                .replaceAll('-', '')
                                                .substring(1);
                                            String textToWa =
                                                Uri.parse('Hi i am MK company')
                                                    .toString();
                                            launch(
                                                'https://wa.me/$phoneNumber?text=$textToWa');
                                          },
                                          child: Container(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 8, horizontal: 16),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  color: Theme.of(context)
                                                      .primaryColor),
                                              child: Row(
                                                children: [
                                                  Icon(
                                                    Icons.phone,
                                                    color: Colors.white,
                                                  ),
                                                  SizedBox(width: 8),
                                                  Text(
                                                    'Phone',
                                                    style: mainFont.copyWith(
                                                        fontSize: 14,
                                                        color: Colors.white),
                                                  ),
                                                ],
                                              )),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            );
            }),
        SizedBox(height: 32)
      ],
    );
  }
}
