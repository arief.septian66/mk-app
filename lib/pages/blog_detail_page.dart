part of 'pages.dart';

class BlogDetailPage extends StatefulWidget {
  final Blog data;
  const BlogDetailPage({Key? key, required this.data}) : super(key: key);

  @override
  State<BlogDetailPage> createState() => _BlogDetailPageState();
}

class _BlogDetailPageState extends State<BlogDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        title: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            children: [
              const Icon(
                Icons.arrow_back,
                color: Colors.black87,
              ),
              SizedBox(width: 15),
              Expanded(
                child: Text(
                  widget.data.title,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: mainFont.copyWith(
                      color: Colors.black87,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 16),
      children: [
        SizedBox(height: 16),
        AspectRatio(
          aspectRatio: 2 / 1,
          child: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                    fit: BoxFit.cover, image: NetworkImage(widget.data.photo))),
          ),
        ),
        SizedBox(height: 16),
        Wrap(
          spacing: 4,
          runSpacing: 4,
          children: List.generate(widget.data.tag.length, (index) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 6),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Theme.of(context).primaryColor),
              child: Text(
                widget.data.tag[index],
                style: mainFont.copyWith(fontSize: 12, color: Colors.white),
              ),
            );
          }),
        ),
        SizedBox(height: 16),
        Text(
          widget.data.title,
          style: mainFont.copyWith(
              fontSize: 22, color: Colors.black87, fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 8),
        Text(
          widget.data.author,
          style: mainFont.copyWith(fontSize: 16, color: Colors.black87),
        ),
        Text(
          dateToReadable(
                  DateFormat('dd-MM-yyyy').format(widget.data.createdAt)) +
              ' ' +
              widget.data.createdAt.toString().substring(11, 16),
          style: mainFont.copyWith(fontSize: 14, color: Colors.black54),
        ),
        SizedBox(height: 16),
        Text(
          widget.data.subTitle,
          style: mainFont.copyWith(fontSize: 14),
        ),
        SizedBox(height: 16),
        Text(
          widget.data.content,
          style: mainFont.copyWith(fontSize: 14),
        ),
        SizedBox(height: 32),
      ],
    );
  }
}
