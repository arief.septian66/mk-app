part of '../pages.dart';

class HomeScreenPage extends StatefulWidget {
  const HomeScreenPage({Key? key}) : super(key: key);

  @override
  State<HomeScreenPage> createState() => _HomeScreenPageState();
}

class _HomeScreenPageState extends State<HomeScreenPage> {
  BlogBloc blogBloc = BlogBloc();
  CandidateBloc candidateBloc = CandidateBloc();
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    blogBloc.intialLoad();
    candidateBloc.intialLoad();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => SearchPage()));
              },
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                    border: Border.all(color: Colors.black12)),
                child: Row(
                  children: [
                    Icon(Icons.search),
                    SizedBox(width: 8),
                    Text('Search..')
                  ],
                ),
              ),
            ),
            Expanded(
                child: ListView(
              padding: horizontalDefaultMargin,
              children: [
                SizedBox(height: 5),
                Text(
                  'Blog',
                  style: mainFont.copyWith(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.black87),
                ),
                SizedBox(height: 10),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: StreamBuilder(
                      stream: blogBloc.blogObservable,
                      builder: (context,
                          AsyncSnapshot<MasterModel<List<Blog>>> snapshot) {
                        return snapshot.data == null
                            ? Container()
                            : snapshot.data!.isLoading
                                ? Row(
                                    children: List.generate(4, (index) {
                                      return Container(
                                        margin: EdgeInsets.only(
                                            left: index == 0 ? 0 : 10),
                                        child: PlaceHolder(
                                          child: Container(
                                            width: 150,
                                            height: 120,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                color: Colors.white),
                                          ),
                                        ),
                                      );
                                    }),
                                  )
                                : snapshot.data!.isError
                                    ? Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Something Wrong, Please Try Again',
                                            style:
                                                mainFont.copyWith(fontSize: 14),
                                          ),
                                          SizedBox(height: 8),
                                          GestureDetector(
                                            onTap: () {
                                              blogBloc.intialLoad();
                                            },
                                            child: Container(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 16, vertical: 8),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  color: Theme.of(context)
                                                      .primaryColor),
                                              child: Text(
                                                'Retry',
                                                style: mainFont.copyWith(
                                                    fontSize: 14,
                                                    color: Colors.white),
                                              ),
                                            ),
                                          )
                                        ],
                                      )
                                    : Row(
                                        children: List.generate(
                                            snapshot.data!.data.length,
                                            (index) {
                                          Blog data =
                                              snapshot.data!.data[index];
                                          return BlogPreviewWidget(
                                            data: data,
                                            index: index,
                                          );
                                        }),
                                      );
                      }),
                ),
                SizedBox(height: 20),
                Text(
                  'Candidates',
                  style: mainFont.copyWith(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.black87),
                ),
                SizedBox(height: 10),
                StreamBuilder(
                    stream: candidateBloc.candidateObservable,
                    builder: (context,
                        AsyncSnapshot<MasterModel<List<Candidate>>> snapshot) {
                      return snapshot.data == null
                          ? Container()
                          : snapshot.data!.isLoading
                              ? Column(
                                  children: List.generate(4, (index) {
                                    return Container(
                                      margin: EdgeInsets.only(
                                          top: index == 0 ? 0 : 10),
                                      child: PlaceHolder(
                                        child: Container(
                                          width: double.infinity,
                                          height: 150,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              color: Colors.white),
                                        ),
                                      ),
                                    );
                                  }),
                                )
                              : snapshot.data!.isError
                                  ? Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Something Wrong, Please Try Again',
                                          style:
                                              mainFont.copyWith(fontSize: 14),
                                        ),
                                        SizedBox(height: 8),
                                        GestureDetector(
                                          onTap: () {
                                            candidateBloc.intialLoad();
                                          },
                                          child: Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 16, vertical: 8),
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                color: Theme.of(context)
                                                    .primaryColor),
                                            child: Text(
                                              'Retry',
                                              style: mainFont.copyWith(
                                                  fontSize: 14,
                                                  color: Colors.white),
                                            ),
                                          ),
                                        )
                                      ],
                                    )
                                  : Column(
                                      children: List.generate(
                                          snapshot.data!.data.length, (index) {
                                        Candidate data =
                                            snapshot.data!.data[index];

                                        return GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (_) =>
                                                        CandidateDetailPage(
                                                            data: data)));
                                          },
                                          child: Column(
                                            children: [
                                              CandidatesInfoWidget(data: data),
                                              Container(
                                                margin: EdgeInsets.only(
                                                    left: 96,
                                                    top: 16,
                                                    bottom: 16),
                                                width: double.infinity,
                                                height: 1,
                                                color: Colors.black12,
                                              )
                                            ],
                                          ),
                                        );
                                      }),
                                    );
                    })
              ],
            ))
          ],
        ),
      ),
    );
  }
}
