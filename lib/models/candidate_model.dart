part of 'models.dart';

class Candidate {
  late int id;
  late String name;
  late String gender;
  late String photo;
  late DateTime birthday;
  late DateTime expired;

  Candidate.fromJson(Map<String, dynamic> jsonMap) {
    id = jsonMap['id'];
    name = jsonMap['name'];
    gender = jsonMap['gender'];
    photo = jsonMap['photo'];
    birthday = DateTime.fromMillisecondsSinceEpoch(jsonMap['birthday']);
    expired = DateTime.fromMillisecondsSinceEpoch(jsonMap['expired']);
  }
}
