part of 'models.dart';

class MasterModel<T> {
  late T data;
  late bool isError;
  late bool isLoading;

  MasterModel(
      {required this.data, required this.isError, required this.isLoading});
}
