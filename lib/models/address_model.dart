part of 'models.dart';

class Address {
  late int id;
  late String address;
  late String city;
  late String state;
  late String zipCode;

  Address.fromJson(Map<String, dynamic> jsonMap) {
    id = jsonMap['id'];
    address = jsonMap['address'];
    city = jsonMap['city'];
    state = jsonMap['state'];
    zipCode = jsonMap['zip_code'].toString();
  }
}
