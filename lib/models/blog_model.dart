part of 'models.dart';

class Blog {
  late int id;
  late String title;
  late String subTitle;
  late String photo;
  late String content;
  late String author;
  late DateTime createdAt;
  late List<String> tag;

  Blog.fromJson(Map<String, dynamic> jsonMap) {
    id = jsonMap['id'];
    title = jsonMap['title'];
    subTitle = jsonMap['subTitle'];
    photo = jsonMap['photo'];
    content = jsonMap['content'];
    author = jsonMap['author'];
    createdAt = DateTime.fromMillisecondsSinceEpoch(jsonMap['create_at']);
    tag = jsonMap['tag'].toString().replaceAll(' ', '').split(',');
  }
}
