part of 'models.dart';

class Experience {
  late int id;
  late String status;
  late String jobTitle;
  late String companyName;
  late String industry;

  Experience.fromJson(Map<String, dynamic> jsonMap) {
    id = jsonMap['id'];
    status = jsonMap['status'];
    jobTitle = jsonMap['job_title'];
    companyName = jsonMap['company_name'];
    industry = jsonMap['industry'];
  }
}
