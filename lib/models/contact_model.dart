part of 'models.dart';

class Contact {
  late int id;
  late String email;
  late String phoneNumber;

  Contact.fromJson(Map<String, dynamic> jsonMap) {
    id = jsonMap['id'];
    email = jsonMap['email'];
    phoneNumber = jsonMap['phone'];
  }
}
